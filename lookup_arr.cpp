#include <iostream>
#include <string>
#include "symboltableArr.hpp"
#include "stopwatch.h"

int main(int argc, char* argv[])
{
  if (argc < 3) {
    std::cerr << "Usage: " << argv[0]
	      << " <min-length> <word>\n";
    exit(1);
  }

  size_t min_length = atoi(argv[1]);
  char * wordSeek = argv[2];
  Stopwatch *timer = new Stopwatch();
  float duration =0.0;
  SymbolTable<std::string, size_t> st;
  
  std::string word = "";
  while (std::cin >> word) {
    
    if (word.size() < min_length)
      continue;

    // if st doesn't contain word, adds default
    // constructed value for word, i.e. 0
    st[word]++;
  }

  timer->start();
  st.contains(wordSeek);
  duration = timer->stop();
  
  std::cout << "The algorithm took " << duration << " seconds" << std::endl;

  return 0;
}
